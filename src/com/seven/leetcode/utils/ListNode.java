package com.seven.leetcode.utils;

public class ListNode {
    public int val;
    public ListNode next;

    public ListNode() {
    }

    public ListNode(int val) {
        this.val = val;
    }

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    @Override
    public String toString() {
        return "ListNode{" +
            "val=" + val +
            ", next=" + next +
            '}';
    }

    public static ListNode fromArray(int[] nums) {
        if (null == nums || nums.length == 0) {
            return null;
        }

        ListNode node = null;
        for (int i = nums.length - 1; i >= 0; i--) {
            int num = nums[i];
            node = new ListNode(num, node);
        }

        return node;
    }
}
