package com.seven.leetcode;

import java.util.concurrent.CompletableFuture;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        String str = "hello";
        //\u000d str += " world";
        System.out.println(str);

        CompletableFuture<Void> res = CompletableFuture.runAsync(() -> {
                System.out.println("hello1");
            })
            .thenAccept((x) -> {
                System.out.println("hello1 success");
            })
            .thenRun(() -> {
                throw new RuntimeException("hello2");
            })
            .thenAccept((x) -> {
                System.out.println("hello2 success");
            })
            .exceptionally(ex -> {
                System.out.println("exception" + ex);
                return null;
            });

        Thread.sleep(1000);
    }
}
