package com.seven.leetcode.problems;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * LongestWordInDictionary
 * https://leetcode-cn.com/problems/longest-word-in-dictionary/
 * 级别：Medium
 * <p>
 * 720. 词典中最长的单词
 * 给出一个字符串数组 words 组成的一本英语词典。返回 words 中最长的一个单词，该单词是由 words 词典中其他单词逐步添加一个字母组成。
 * <p>
 * 若其中有多个可行的答案，则返回答案中字典序最小的单词。若无答案，则返回空字符串。
 * <p>
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * 输入：words = ["w","wo","wor","worl", "world"]
 * 输出："world"
 * 解释： 单词"world"可由"w", "wo", "wor", 和 "worl"逐步添加一个字母组成。
 * 示例 2：
 * <p>
 * 输入：words = ["a", "banana", "app", "appl", "ap", "apply", "apple"]
 * 输出："apple"
 * 解释："apply" 和 "apple" 都能由词典中的单词组成。但是 "apple" 的字典序小于 "apply"
 * <p>
 * <p>
 * 提示：
 * <p>
 * 1 <= words.length <= 1000
 * 1 <= words[i].length <= 30
 * 所有输入的字符串 words[i] 都只包含小写字母。
 *
 * @author Carl
 * @date 2022/03/17 22:22
 */
public class LongestWordInDictionary {

    public static void main(String[] args) {
        String[] words = {"rac", "rs", "ra", "on", "r", "otif", "o", "onpdu", "rsf", "rs", "ot", "oti", "racy", "onpd"};
        //String[] words = {"r","kt","jtgt","j","jtg","rdwy","chkext","c","l","zo","lnp","k","jt","chke","ktui","rd","jtgtha","ch","chkex"};
        //String[] words = {"k","lg","it","oidd","oid","oiddm","kfk","y","mw","kf","l","o","mwaqz","oi","ych","m","mwa"};
        //String[] words = {"m","mo","moc","moch","mocha","l","la","lat","latt","latte","c","ca","cat"};
        //String[] words = {"a", "banana", "app", "appl", "ap", "apply", "apple"};
        //String[] words = {"w","wo","wor","worl", "world"};
        System.out.println("in:\nwords: " + Arrays.toString(words));
        String result = longestWord2(words);
        System.out.println("out: " + result);
    }

    public static String longestWord(String[] words) {
        String ans = "";
        Set<String> set = new HashSet<>(Arrays.asList(words));

        for (String s : set) {
            int n = s.length(), m = ans.length();
            if (n < m) {
                continue;
            }
            if (n == m && s.compareTo(ans) > 0) {
                continue;
            }
            boolean ok = true;
            for (int i = 1; i <= n && ok; i++) {
                String sub = s.substring(0, i);
                if (!set.contains(sub)) {
                    ok = false;
                }
            }
            if (ok) {
                ans = s;
            }
        }
        return ans;
    }

    static int N = 30010, M = 26;
    static int[][] tr = new int[N][M];
    static boolean[] isEnd = new boolean[N];
    static int idx = 0;

    static void add(String s) {
        int p = 0, n = s.length();
        for (int i = 0; i < n; i++) {
            int u = s.charAt(i) - 'a';
            if (tr[p][u] == 0) {
                tr[p][u] = ++idx;
            }
            p = tr[p][u];
        }
        isEnd[p] = true;
    }

    static boolean query(String s) {
        int p = 0, n = s.length();
        for (int i = 0; i < n; i++) {
            int u = s.charAt(i) - 'a';
            p = tr[p][u];
            if (!isEnd[p]) {
                return false;
            }
        }
        return true;
    }

    public static String longestWord2(String[] words) {
        Arrays.fill(isEnd, false);
        for (int i = 0; i <= idx; i++) {
            Arrays.fill(tr[i], 0);
        }
        idx = 0;

        String ans = "";
        for (String s : words) {
            add(s);
        }
        for (String s : words) {
            int n = s.length(), m = ans.length();
            if (n < m) {
                continue;
            }
            if (n == m && s.compareTo(ans) > 0) {
                continue;
            }
            if (query(s)) {
                ans = s;
            }
        }
        return ans;
    }
}
