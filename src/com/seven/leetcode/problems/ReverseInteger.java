package com.seven.leetcode.problems;

/**
 * 7. 整数反转
 * ReverseInteger
 * https://leetcode-cn.com/problems/reverse-integer/
 * 级别：Easy
 * <p>
 * 给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。
 * <p>
 * 如果反转后整数超过 32 位的有符号整数的范围[−2^31, 2^31− 1] ，就返回 0。
 * <p>
 * 假设环境不允许存储 64 位整数（有符号或无符号）。
 * <p>
 * 示例 1：
 * <p>
 * 输入：x = 123
 * 输出：321
 * 示例 2：
 * <p>
 * 输入：x = -123
 * 输出：-321
 * 示例 3：
 * <p>
 * 输入：x = 120
 * 输出：21
 * 示例 4：
 * <p>
 * 输入：x = 0
 * 输出：0
 *
 * @author : wenguang
 * @date : 2021/3/22 9:26
 */
public class ReverseInteger {

    public static void main(String[] args) {
        int target = 2100000018;
        System.out.println("in:\ntarget: " + target);
        int result = reverseInteger(target);
        System.out.println("out: " + result);
    }

    public static int reverseInteger(int x) {
        int ans = 0;
        while (x != 0) {
            if ((ans * 10) / 10 != ans) {
                ans = 0;
                break;
            }
            ans = ans * 10 + x % 10;
            x = x / 10;
        }
        return ans;
    }
}
