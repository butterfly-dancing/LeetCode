package com.seven.leetcode.problems;

/**
 * 633. 平方数之和
 * https://leetcode-cn.com/problems/sum-of-square-numbers/
 * 级别：Medium
 * <p>
 * 给定一个非负整数 c ，你要判断是否存在两个整数 a 和 b，使得 a2 + b2 = c 。
 * <p>
 * 示例 1：
 * 输入：c = 5
 * 输出：true
 * 解释：1 * 1 + 2 * 2 = 5
 * <p>
 * 示例 2：
 * 输入：c = 3
 * 输出：false
 * <p>
 * 示例 3：
 * 输入：c = 4
 * 输出：true
 * <p>
 * 示例 4：
 * 输入：c = 2
 * 输出：true
 * <p>
 * 示例 5：
 * 输入：c = 1
 * 输出：true
 * <p>
 * 提示：
 * 0 <= c <= 231 - 1
 *
 * @author : wenguang
 * @date : 2021/4/28 9:55
 */
public class SumOfSquareNumbers {

    public static void main(String[] args) {
        int num = 1;
        System.out.println("in:\nnum: " + num);
        boolean res = judgeSquareSum(num);
        System.out.println("out: " + res);
    }

    public static boolean judgeSquareSum(int c) {
        int sqrt = (int) Math.sqrt(c);

        int i = 0;
        int x = 0;
        int y = 0;
        while (i <= sqrt) {
            x = c - i * i;
            y = (int) Math.sqrt(x);
            if (x == y * y) {
                return true;
            }
            i++;
        }

        return false;
    }
}
