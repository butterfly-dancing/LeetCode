package com.seven.leetcode.problems;

import java.util.Arrays;

/**
 * 80. 删除有序数组中的重复项 II
 * RemoveDuplicatesFromSortedArray2
 * https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array-ii/
 * 级别：Medium
 * <p>
 * 给你一个有序数组 nums ，请你 原地 删除重复出现的元素，使每个元素 最多出现两次 ，返回删除后数组的新长度。
 * 不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。
 * <p>
 * 说明：
 * <p>
 * 为什么返回数值是整数，但输出的答案是数组呢？
 * <p>
 * 请注意，输入数组是以「引用」方式传递的，这意味着在函数里修改输入数组对于调用者是可见的。
 * <p>
 * 你可以想象内部操作如下:
 * <p>
 * // nums 是以“引用”方式传递的。也就是说，不对实参做任何拷贝
 * int len = removeDuplicates(nums);
 * <p>
 * // 在函数里修改输入数组对于调用者是可见的。
 * // 根据你的函数返回的长度, 它会打印出数组中 该长度范围内 的所有元素。
 * for (int i = 0; i < len; i++) {
 * print(nums[i]);
 * }
 * <p>
 * 示例 1：
 * 输入：nums = [1,1,1,2,2,3]
 * 输出：5, nums = [1,1,2,2,3]
 * 解释：函数应返回新长度 length = 5, 并且原数组的前五个元素被修改为 1, 1, 2, 2, 3 。 不需要考虑数组中超出新长度后面的元素。
 * <p>
 * 示例 2：
 * 输入：nums = [0,0,1,1,1,1,2,3,3]
 * 输出：7, nums = [0,0,1,1,2,3,3]
 * 解释：函数应返回新长度 length = 7, 并且原数组的前五个元素被修改为 0, 0, 1, 1, 2, 3, 3 。 不需要考虑数组中超出新长度后面的元素。
 *
 * @author : wenguang
 * @date : 2021/3/22 9:26
 */
public class RemoveDuplicatesFromSortedArray2 {

    public static void main(String[] args) {
        int[] nums = new int[]{0, 0, 1, 1, 1, 1, 2, 3, 3};
        System.out.println("in: \nnums->" + Arrays.toString(nums));
        int result = removeDuplicatesFromSortedArray(nums);
        System.out.println("out: " + result + "\nnums->" + Arrays.toString(nums));
    }

    public static int removeDuplicatesFromSortedArray(int[] nums) {
        if (null == nums) {
            return 0;
        }
        if (nums.length < 2) {
            return nums.length;
        }

        int len = nums.length;
        for (int j = 1; j < len - 1; j++) {
            int num = nums[j];
            if (num == nums[j - 1] && num == nums[j + 1]) {
                if (len - 1 - (j + 1) >= 0) {
                    System.arraycopy(nums, j + 2, nums, j + 1, len - 1 - (j + 1));
                }
                j--;
                len--;
            }
        }

        return len;
    }
}
