package com.seven.leetcode.problems;

import java.util.Arrays;

/**
 * 179. 最大数
 * https://leetcode-cn.com/problems/two-sum
 * 级别：Medium
 * <p>
 * 给定一组非负整数 nums，重新排列每个数的顺序（每个数不可拆分）使之组成一个最大的整数。
 * <p>
 * 注意：输出结果可能非常大，所以你需要返回一个字符串而不是整数。
 * <p>
 * 示例 1：
 * <p>
 * 输入：nums = [10,2]
 * 输出："210"
 * 示例 2：
 * <p>
 * 输入：nums = [3,30,34,5,9]
 * 输出："9534330"
 * 示例 3：
 * <p>
 * 输入：nums = [1]
 * 输出："1"
 * 示例 4：
 * <p>
 * 输入：nums = [10]
 * 输出："10"
 * <p>
 * <p>
 * 提示：
 * <p>
 * 1 <= nums.length <= 100
 * 0 <= nums[i] <= 109
 *
 * @author : wenguang
 * @date : 2021/3/22 9:26
 */
public class LargestNumber {

    public static void main(String[] args) {
        int[] nums = new int[]{3, 30, 34, 5, 9};
        System.out.println("in: " + Arrays.toString(nums));
        String result = largestNumber(nums);
        System.out.println("out: " + result);
    }

    public static String largestNumber(int[] nums) {
        //合法性
        if (nums == null || nums.length == 0) {
            return "";
        }
        //数字数组->字符数组  转化
        String[] strArr = new String[nums.length];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = Integer.toString(nums[i]);
        }
        //重写排序规则 12-14ms
        Arrays.sort(strArr, (o1, o2) -> {
            //继承此方法的时候，要自定义比较器，conpareTo方法返回值为1(升序),0，-1(降序)。
            //返回正值 交换；负值不交换
            return (o2 + o1).compareTo((o1 + o2));
        });

        //字符数组->字符串 转化
        StringBuilder sb = new StringBuilder();
        for (String aStrArr : strArr) {
            sb.append(aStrArr);
        }
        String result = sb.toString();
        //特殊情况 若干个零
        if (result.charAt(0) == '0') {
            result = "0";
        }
        return result;
    }
}
