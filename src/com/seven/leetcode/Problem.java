/*
 * Copyright (c) 2020. Carl
 */
package com.seven.leetcode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Problem {

    public static void main(String[] args) throws IOException {
        InputStream in = System.in;
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String[] params = br.readLine().trim().split(" ");
        int c = Integer.parseInt(params[0]), x = Integer.parseInt(params[1]);
        params = br.readLine().trim().split(" ");

        int[] arr = new int[c];
        for (int i = 0; i < c; i++) {
            arr[i] = Integer.parseInt(params[i]);
        }

        long sum = 0, count = 0;
        int j = 0;
        for (int i = 0; i < c; i++) {
            sum += arr[i];
            while (sum >= x && j <= i) {
                count += c - i;
                sum -= arr[j];
                j++;
            }
        }

        System.out.println(count);
    }

    public static void main3(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            String s1 = in.nextLine();
            String s2 = in.nextLine();

            String[] para = s1.split(" ");
            String[] strings = s2.split(" ");
            List<Integer> integers = Arrays.stream(strings)
                .map(Integer::parseInt)
                .collect(Collectors.toList());
            long count = findCount(Integer.parseInt(para[0]), Integer.parseInt(para[1]), integers);
            System.out.println(count);
        }
    }

    private static long findCount(int len, int x, List<Integer> nums) {
        long sum = 0, count = 0;
        int j = 0;

        for (int i = 0; i < len; i++) {
            sum += nums.get(i);
            while (sum >= x && j <= i) {
                count += len - 1;
                sum -= nums.get(j);
                j++;
            }
        }

        return count;
    }


    public static void main2(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            String target = in.nextLine();
            String source = in.nextLine();

            //abc
            //abcaybec
            int res = findSubstring(target, source);
            System.out.println(res);
        }
    }

    private static int findSubstring(String target, String source) {
        if (null == target || null == source) {
            return 0;
        }

        int tLen = target.length();
        int sLen = source.length();

        if (tLen > sLen) {
            return -1;
        }

        if (tLen == 0) {
            return -1;
        }

        char[] tChars = target.toCharArray();
        char[] sChars = source.toCharArray();
        int tIdx = tLen - 1;
        int sIdx = sLen - 1;
        while (sIdx >= 0) {
            if (tChars[tIdx] == sChars[sIdx]) {
                tIdx--;

                if (tIdx == -1) {
                    return sIdx;
                }
            }
            sIdx--;
        }

        return -1;
    }

    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            String a = in.nextLine();

            String[] nums = a.split(",");
            Arrays.sort(nums, (x, y) -> (y + x).compareTo(x + y));
            String collect = String.join("", nums);
            System.out.println(collect);
        }
    }
}
